'use strict';

(function() {
  var client = algoliasearch('BVRXQQ5AXI', '11a880fac34cb618ff865f2899543f7c');
  var helper = algoliasearchHelper(client, 'restaurants', {
    hitsPerPage: 3,
    facets: ['food_type', 'stars_count_integer', 'payment_options'],
    disjunctiveFacets: ['food_type', 'stars_count_integer', 'payment_options']
  });

  helper.on('result', renderHits);

  // Improve results with geolocation data if available
  // Otherwise use IP geolocation provided by Algolia helper
  if ('geolocation' in navigator) {
    navigator.geolocation.watchPosition(function(position) {
      helper.setQueryParameter('aroundLatLng',
        position.coords.latitude + ', ' + position.coords.longitude);
      helper.setQueryParameter('aroundLatLngViaIP', false);
    }, function (error) {
      console.log('Error with geolocation API, using IP fallback instead');
      helper.setQueryParameter('aroundLatLngViaIP', true);
    }, {
      enableHighAccuracy: true,
      timeout: 60000,
      maximumAge: 300000
    });
  } else {
    helper.setQueryParameter('aroundLatLngViaIP', true);
  }

  // don't send more than 1 query per 200 ms
  var sendQuery = (function(delay) {
    var timeout;
    
    return function(query) {
      clearTimeout(timeout);

      timeout = setTimeout(function() {
        timeout = null;
        helper.setQuery(query);
        helper.search();
      }, delay);
    };

  }) (200);

  $('#search-form').on('submit', function(event) {
    event.preventDefault();
  });

  $('#search-input').on('input', function(event) {
    event.preventDefault();
    sendQuery(this.value)
  });

  $('#more').click(function() {
    helper.nextPage();
    helper.search();
  });

  function renderHits(content) {
    $('#nb-hits').text(content.nbHits);
    $('#processing-time').text(content.processingTimeMS / 1000);
    $('#search-results').slideDown();
    $('#button-filters-wrapper').show();

    var fragment = $(document.createDocumentFragment());

    content.hits.forEach(function(hit) {
      fragment.append('<li class="restaurant-result clearfix">' + 
        '<img alt="" class="restaurant-photo float-left" src="' +
          hit.image_url + '" width="128" height="128">' +
        '<h2 class="restaurant-name">' + hit.name + '</h2>' +
        generateStarRating(hit.stars_count) +
        '<span class="detail">(' + hit.reviews_count + ' reviews)</span>' +
        '<div class="detail restaurant-details">' +
          hit.food_type + ' | ' + hit.neighborhood + ' | ' + hit.price_range +
        '</div>' +
      '</li>');
    });

    var resultsElement = $('#results');
    if (content.page === 0) {
      resultsElement.empty();
    }
    resultsElement.append(fragment);

    // only show "more" button if not at end of results
    if (content.nbPages === content.page + 1) {
      $('#more').hide();
    } else {
      $('#more').show();
    }

    renderFacetList(content);
  }

  function renderFacetList(content) {
    var fragment = $(document.createDocumentFragment());

    // food_type facets
    content.getFacetValues('food_type').forEach(function(type) {
      fragment.append('<label class="filter-button' + (type.isRefined ? ' active' : '') + '">' +
        '<input type="checkbox" data-facet="food_type" data-value="' + type.name + '"' +
          (type.isRefined ? ' checked' : '') + '>' +
        '<span class="filter-count">' + type.count + '</span>' +
        type.name +
      '</label>');
    });

    fragment.append(generateShowAllButton());

    $('#filter-type').empty().append(fragment);

    fragment.empty();

    // stars_count_integer facets
    var ratings = content.getFacetValues('stars_count_integer', {
      sortBy: ['name:desc']
    });
    ratings.forEach(function(rating) {
      fragment.append(generateFacetStars(rating.name, rating.count, rating.isRefined));
    });

    fragment.append(generateShowAllButton());

    $('#filter-rating').empty().append(fragment);

    fragment.empty();

    // payment_options facets
    content.getFacetValues('payment_options').forEach(function(option) {
      fragment.append('<label class="filter-button' + (option.isRefined ? ' active' : '') + '">' +
        '<input type="checkbox" data-facet="payment_options" data-value="' + option.name + '"' +
          (option.isRefined ? ' checked' : '') + '>' +
        '<span class="filter-count">' + option.count + '</span>' +
        option.name +
      '</label>');
    });

    fragment.append(generateShowAllButton());

    $('#filter-payment').empty().append(fragment);
  }

  $('#filters').on('change', '.filter-button > input', updateFilters);

  function generateStarRating(rating) {
    return '<span class="rating" aria-hidden="true">' + rating +
      generateStars(rating) +
    '</span>';
  }

  function generateFacetStars(rating, count, isRefined) {
    return '<label class="rating filter-button' + (isRefined ? ' active' : '') + '">' +
      '<input type="checkbox" data-facet="stars_count_integer" data-value="' + rating + '"' + 
        (isRefined ? ' checked' : '') + '>' +
      '<span class="filter-count">' + count + '</span>' +
      generateStars(rating) +
      '<span class="sr-only">' + rating + ' stars</span>' +
    '</label>';
  }

  function generateStars(rating) {
    return '<span class="rating-stars">' + 
      '<span class="rating-stars-inner" style="width:' + 20*rating + '%"></span>' +
    '</span>';
  }

  function generateShowAllButton() {
    return $('<button class="filter-showall button-more">Show all</button>')
    .click(function() {
      $(this).parent().children('.filter-button').css('display', 'block');
      $(this).remove();
    });
  }

  function updateFilters() {
    var button = $(this).parent();
    if (this.checked) {
      button.addClass('active');
      helper.addFacetRefinement($(this).data('facet'), $(this).data('value'));
    } else {
      button.removeClass('active');
      helper.removeFacetRefinement($(this).data('facet'), $(this).data('value'));
    }
    helper.search();
  }
}) ();

// Show/hide filters (mobile)
$('#button-filters').click(function() {
  if ($('#filters').is(':visible')) {
    $(this).text('Filter results');
    $('#filters').slideUp();
  } else {
    $(this).text('Hide filters');
    $('#filters').slideDown();
  }
});

$(window).on('resize', function() {
  $('#filters').attr('style', '');
});