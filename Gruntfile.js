module.exports = function(grunt) {

  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    postcss: {
      options: {
        processors: [
          require('autoprefixer')({browsers: ['> 1%', 'iOS 7']}),
          require('cssnano')()
        ]
      },
      dist: {
        files: {
          'dist/css/main.css': 'dist/css/main.css'
        }
      }
    },
    sass: {
      dist: {
        files: {
          'dist/css/main.css': 'src/scss/main.scss'
        }
      }
    },
    uglify: {
      app: {
        files: {
          'dist/app.js': [
            'src/js/app.js',
            'node_modules/lodash.debounce/index.js'
          ]
        }
      }
    },
    watch: {
      css: {
        files: ['src/scss/*.scss'],
        tasks: ['css']
      },
      js: {
        files: ['src/js/*.js'],
        tasks: ['js']
      }
    }
  });
  grunt.registerTask('css', ['sass', 'postcss']);
  grunt.registerTask('js', ['uglify']);
  grunt.registerTask('default', ['css', 'js']);
  grunt.registerTask('dev', ['default', 'watch']);
}