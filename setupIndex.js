var algoliasearch = require('algoliasearch');
var fs = require('fs');
var csvParse = require('csv-parse/lib/sync');
var uniq = require('lodash.uniq');

var client = algoliasearch('BVRXQQ5AXI', '094fe55b702f9115d9bfe22fc033b3d7');
var index = client.initIndex('restaurants');
index.setSettings({
  attributesForFaceting: ['searchable(food_type)', 'stars_count_integer', 'payment_options'],
  maxFacetHits: 100
});

// push CSV data
// add stars_count_integer for faceting (better to have 6 values than 51!)
fs.readFile('resources/dataset/restaurants_info.csv', 'utf8', function(error, data) {
  if (error) {
    console.log(error);
    return;
  }
  data = csvParse(data, { delimiter: ';', columns: true })
  .map(function(record) {
    record.stars_count_integer = Math.round(parseFloat(record.stars_count, 10));
    return record;
  });
  index.partialUpdateObjects(data);
});

// push JSON data
// for payment_options:
//    rename AMEX to American Express
//    group Diners Club and Carte Blanche into Discover
//    remove payment options besides these, MasterCard and Visa
fs.readFile('resources/dataset/restaurants_list.json', 'utf8', function(error, data) {
  if (error) {
    console.log(error);
    return;
  }
  data = JSON.parse(data)
  .map(function(record) {
    record.payment_options = uniq(record.payment_options.filter(function(value) {
      return value !== 'JCB' && value !== 'Pay with OpenTable' && value !== 'Cash Only';
    }).map(function(value) {
      if (value === 'AMEX') {
        return 'American Express';
      } else if (value === 'Diners Club' || value === 'Carte Blanche') {
        return 'Discover';
      } else {
        return value;
      }
    }));
    return record;
  });
  index.partialUpdateObjects(data);
});